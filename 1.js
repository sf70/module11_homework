//Дан массив. Нужно вывести в консоль количество чётных и нечётных элементов в массиве. Если в массиве есть нулевой элемент, то он учитывается и выводится отдельно. 

let t = [1,2,3,4,5,6,7,8,9,0,null,'bla', true]
let z = 0; 
let even = 0;
let odd = 0;
arrayParser(t);

function arrayParser(array) {
  for(i=0; i<=array.length-1; i++) {
    if (typeof(array[i]) === 'number') {
      switch (true) {
        case (isNaN(array[i])):
          break;
        case (array[i] === 0):
          z++;
          break;
        case (array[i]%2 == 0):
          even++;
          break;
        default:
          odd++;
      }
    }
  }
}


console.log('В массиве\n' + z + ' нулей,\n' + even + ' четных\n' + odd + ' нечетных чисел');

