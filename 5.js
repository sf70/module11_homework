//Напишите функцию, которая принимает два натуральных числа x и n и возвращает x в степени n. Иначе говоря, умножает x на себя n раз и возвращает результат.

// Используйте Arrow Function синтаксис.

const fatBodyArrowFunction = (a, b) => {
  let result = 1
  if (typeof(a) === 'number' && typeof(b) === 'number') {
    for (let i = 1; i<=b; i++) {
      result *= a;
    }
    console.log(result);
  }
};

fatBodyArrowFunction(2, 10);
