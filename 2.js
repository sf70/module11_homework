//Напишите функцию, которая принимает на входе любое число (но не больше 1 000), определяет, является ли оно простым, и выводит, простое число или нет. Если введено больше 1 000, то выводится сообщение, что данные неверны. Обратите внимание на числа 0 и 1.

for (let i=0; i<100; i++) {
  let n = Math.floor(Math.random() * 2000);
  let answer = primeNumberDetector(n)
  if (typeof(answer) !== 'boolean') {
    continue;
  }
  if (answer) {
    console.log('Число ' + n + ' простое ✅✅✅');
  } else {
    console.log('Число ' + n + ' не простое ❌❌❌');
  }
}

function primeNumberDetector(num) {
  if (typeof(num) === 'number') {
    switch (true) {
      case num > 1000:
        console.log("Вы прислали " + num + ". Данные неверны! 💩💩💩");
        return "error";
        // break;
      case num === 0 || num === 1:
        return false;
      case num === 2:
        return true;
      default:
        for(i = 2; i <= num; i++) {
          if (num%i === 0 && i !== num) {
            return false;
          }
          if (i === num) {
            return true;
          }
        }
    }
  } else {
    return "Вы прислали не число: " + num + "!";
  }
}

